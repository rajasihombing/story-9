from django.test import LiveServerTestCase, TestCase, tag
from django.urls import resolve
from django.test import TestCase , Client
from django.apps import apps
from .apps import MainConfig
from .views import index,user_login, user_loginsuccess,user_logout,user_signup

class UnitTestku(TestCase):

    def test_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main') 

    def test_get(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Welcome", html_kembalian)
        self.assertIn("Login", html_kembalian)
        self.assertIn("Sign Up", html_kembalian)
          
    def test_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_view_login(self):
        found = resolve('/login')
        self.assertEqual(found.func, user_login)
    
    def test_view_loginsukses(self):
        found = resolve('/loginsuccess')
        self.assertEqual(found.func, user_loginsuccess)
    
    def test_view_logout(self):
        found = resolve('/logout')
        self.assertEqual(found.func, user_logout)

    def test_view_signup(self):
        found = resolve('/signup')
        self.assertEqual(found.func, user_signup)
    
    


